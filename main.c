///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// Actual Output:
/// 48723737032
/// 45898811137
/// 55111888863
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   // Sum array1[] with a for() loop and print the result
   long sum = 0;
   int i = 0;
   for( i = 0; i < 100; i++){
      sum += threeArrays.array1[i];
   }
   
   printf("%ld\n", sum);

   // Sum array2[] with a while() { } loop and print the result
   sum = 0;
   i = 0;
   while(i < 100){
      sum += threeArrays.array2[i++];
   }
   
   printf("%ld\n", sum);

   // Sum array3[] with a do { } while() loop and print the result
   sum = 0;
   i = 0;
   do{
      sum += threeArrays.array3[i++];
   }while(i <= 100);
   
   printf("%ld\n", sum);

	return 0;
}

